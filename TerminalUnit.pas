unit TerminalUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.Edit, FMX.Layouts, FMX.Memo, FMX.StdCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdGlobal, Posix.Systime, ButtonThread,ButtonThread_TIGAC,
  IdUDPBase, IdUDPClient, Data.Bind.EngExt, Fmx.Bind.DBEngExt, System.Rtti,
  System.Bindings.Outputs, Androidapi.JNI.PowerManager, Fmx.Bind.Editors, Data.Bind.Components, Androidapi.JNI.Os;

type
  TForm1 = class(TForm)
    IDC: TIdTCPClient;
    ScrollBox1: TScrollBox;
    Memo1: TMemo;
    IP: TEdit;
    Button_Send: TButton;
    Switch1: TSwitch;
    Port: TEdit;
    Command: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button_OpenGas: TButton;
    Button_CloseGas: TButton;
    L1: TLabel;
    L_firmware: TLabel;
    tb_tImax: TTrackBar;
    L_tIMax: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    tb_tImin: TTrackBar;
    L_tIMin: TLabel;
    Label4: TLabel;
    tb_IMin: TTrackBar;
    L_IMin: TLabel;
    Label6: TLabel;
    tb_Imax: TTrackBar;
    L_IMax: TLabel;
    Label8: TLabel;
    tb_tCloseGas: TTrackBar;
    L_tCloseGas: TLabel;
    Label10: TLabel;
    tb_tStartPWM: TTrackBar;
    L_tStartPWM: TLabel;
    rbTIG_DC: TRadioButton;
    rbTIG_AC: TRadioButton;
    RadioButton3: TRadioButton;
    tb_Freq: TTrackBar;
    tb_Bal: TTrackBar;
    tb_Iclean: TTrackBar;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    L_Freq: TLabel;
    L_Balance: TLabel;
    L_IClean: TLabel;
    stateWord: TLabel;
    function sendCommand(const s:String): String;
    function readanswer:string;
    procedure IDCConnected(Sender: TObject);
    procedure Button_SendClick(Sender: TObject);
    procedure Switch1Switch(Sender: TObject);
    procedure IDCDisconnected(Sender: TObject);
    procedure CommandKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure SetCommandStatus(CommandStatus: boolean);
    procedure OpenGas;
    procedure CloseGas;
    procedure StartPWM;
    procedure StartAC;
    procedure StopPWM;
    procedure SetI(I: integer);
    procedure SetBal(Bal: integer);
    procedure SetFreq(Freq: integer);
    procedure SetIClean(I: integer);
    procedure tb_tImaxChange(Sender: TObject);
    procedure tb_tIminChange(Sender: TObject);
    procedure tb_IMinChange(Sender: TObject);
    procedure tb_ImaxChange(Sender: TObject);
    procedure tb_tCloseGasChange(Sender: TObject);
    procedure tb_tStartPWMChange(Sender: TObject);
    procedure ChangeParams;
    procedure FormActivate(Sender: TObject);
    procedure tb_IcleanChange(Sender: TObject);
    procedure tb_BalChange(Sender: TObject);
    procedure tb_FreqChange(Sender: TObject);
    procedure rbTIG_DCClick(Sender: TObject);
    procedure rbTIG_ACClick(Sender: TObject);
  private
//    FWakeLock: JPowerManager_WakeLock;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  thr: TButtonThread;
  thr2: TButtonThread_TIGAC;
  Form1: TForm1;
  isGasOpen: boolean = false;
  isCommandSended: boolean = false;
  TV,TV1:Timeval;
  sendedCmd:string;
  ChangeParamsFlag:boolean=false;
  oldImin: double = 5;

implementation

{$R *.fmx}

function TForm1.sendCommand(const s:String): string;
begin
  IDC.IOHandler.Write(s+#13);
  sendedCmd:=s; //debug
  result:= readanswer;
  //isCommandSended:=true;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  gettimeofday(TV, nil);
  //timer1.Enabled:=not timer1.Enabled;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  thr:= TButtonThread.Create(false);
//  thr.Start;
end;

procedure TForm1.Button_SendClick(Sender: TObject);
begin
  sendCommand(Command.Text);
  memo1.Lines.Add(readAnswer);
  memo1.SelStart := memo1.Text.Length;
  memo1.SelLength := 0;
  memo1.ScrollBy(0, memo1.Lines.Count);
  Command.SelectAll;
  Command.DeleteSelection;
end;

procedure TForm1.rbTIG_ACClick(Sender: TObject);
begin
  Label3.Visible:=true;
  Label5.Visible:=true;
  Label7.Visible:=true;
  L_IClean.Visible:=true;
  L_Balance.Visible:=true;
  L_Freq.Visible:=true;
  tb_Iclean.Visible:=true;
  tb_Freq.Visible:=true;
  tb_Bal.Visible:=true;
  if tb_IMin.Value<10 then oldImin:=tb_IMin.Value
  else oldImin:=-1;
  tb_IMin.Min:=10;
end;

procedure TForm1.rbTIG_DCClick(Sender: TObject);
begin
  Label3.Visible:=false;
  Label5.Visible:=false;
  Label7.Visible:=false;
  L_IClean.Visible:=false;
  L_Balance.Visible:=false;
  L_Freq.Visible:=false;
  tb_Iclean.Visible:=false;
  tb_Freq.Visible:=false;
  tb_Bal.Visible:=false;
  if (oldImin<10)and(oldIMin>=0) then tb_IMin.Value:=oldIMin;
  tb_IMin.Min:=5;
end;

function TForm1.readAnswer:string;
var rcvByte:byte;
begin
  result:='';
//  result:=IDC.IOHandler.ReadLn(#13);
  try
    rcvByte:=IDC.IOHandler.ReadByte;
    while rcvByte<>13 do
    begin
      result:=result+chr(rcvByte);
      rcvByte:=IDC.IOHandler.ReadByte;
    end;
  Except
    on E : Exception do
    begin
      result:='read timeout';
    end;
  end;
  L1.Text:= result;
//  memo1.Lines.Add(sendedCmd);
//  memo1.Lines.Add(result);
//  memo1.SelStart := memo1.Text.Length;
//  memo1.SelLength := 0;
//  memo1.ScrollBy(0, memo1.Lines.Count);
end;

procedure TForm1.SetCommandStatus(CommandStatus: boolean);
begin
  isCommandSended:=CommandStatus;
end;

procedure TForm1.SetBal(Bal: integer);
begin
    sendCommand('b'+inttostr(Bal));
end;

procedure TForm1.SetFreq(Freq: integer);
begin
  sendCommand('f'+inttostr(Freq));
end;

procedure TForm1.SetI(I: integer);
begin
  sendCommand('a'+inttostr(I));
end;

procedure TForm1.SetIClean(I: integer);
begin
  sendCommand('c'+inttostr(I));
end;

procedure TForm1.StartPWM;
begin
  sendCommand('s');
end;

procedure TForm1.StartAC;
begin
  sendCommand('~');
end;

procedure TForm1.StopPWM;
begin
  sendCommand('e');
end;

procedure TForm1.CloseGas;
begin
  sendCommand('r');
end;

procedure TForm1.OpenGas;
begin
  sendCommand('g');
end;

procedure TForm1.Switch1Switch(Sender: TObject);
begin
  if Switch1.IsChecked then
  begin
    IDC.Host:=IP.Text;
    IDC.Port:=strtoint(Port.Text);
    try
      // TIG DC:
        IDC.Connect;

//      else Switch1.IsChecked:=false;
    except
      on E : Exception do
      begin
        L1.Text:= E.ClassName+' message: '+E.Message;
//        memo1.Lines.Add(E.ClassName+'message: '+E.Message);
        Switch1.IsChecked:=false;

      end;
    end;
    IDC.IOHandler.ReadTimeout:=10000;
  end else
  try
    if rbTIG_DC.IsChecked then
    begin
      thr.Terminate;
      rbTIG_DC.Enabled:=true;
      rbTIG_AC.Enabled:=true;
      RadioButton3.Enabled:=true;
    end else
    if rbTIG_AC.IsChecked then
    begin
      thr2.Terminate;
      rbTIG_DC.Enabled:=true;
      rbTIG_AC.Enabled:=true;
      RadioButton3.Enabled:=true;
    end;

  except
      on E : Exception do
      begin

      end;
  end;
end;

procedure TForm1.tb_BalChange(Sender: TObject);
var st:string;
begin
  st:=floattostrF(tb_Bal.Value, ffFixed, 4, 0);
  L_Balance.Text:=st;
  ChangeParams;
end;

procedure TForm1.tb_FreqChange(Sender: TObject);
var st:string;
begin
  st:=floattostrF(tb_Freq.Value, ffFixed, 4, 0);
  L_Freq.Text:=st;
  ChangeParams;
end;

procedure TForm1.tb_IcleanChange(Sender: TObject);
var st:string;
begin
  st:=floattostrF(tb_Iclean.Value, ffFixed, 4, 0);
  L_IClean.Text:=st;
  ChangeParams;
end;

procedure TForm1.tb_ImaxChange(Sender: TObject);
var st:string;
begin
  st:=floattostr(tb_Imax.Value);
  if st<>'' then
    L_IMax.Text:=st;
  ChangeParams;
end;

procedure TForm1.tb_IMinChange(Sender: TObject);
var st:string;
begin
  st:=floattostr(tb_Imin.Value);
  if st<>'' then
    L_IMin.Text:=st;
  ChangeParams;
end;

procedure TForm1.tb_tCloseGasChange(Sender: TObject);
var st:string;
begin
  st:=FloatToStrF(tb_tCloseGas.Value,ffFixed,4,1);
  L_tCloseGas.Text:=st;
  ChangeParams;
end;

procedure TForm1.tb_tImaxChange(Sender: TObject);
var st:string;
begin
  st:=FloatToStrF(tb_tImax.Value,ffFixed,4,1);
  L_tIMax.Text:=st;
  ChangeParams;
end;

procedure TForm1.ChangeParams;
begin
  if not ChangeParamsFlag then
  begin
    ChangeParamsFlag:=true;
  end;
end;

procedure TForm1.CommandKeyDown(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if key=13 then
    Button_SendClick(self);
end;

procedure TForm1.FormActivate(Sender: TObject);
begin
  tb_tImaxChange(self);
  tb_tIminChange(self);
  tb_IMinChange(self);
  tb_ImaxChange(self);
  tb_tCloseGasChange(self);
  tb_tStartPWMChange(self);
  tb_IcleanChange(self);
  tb_BalChange(self);
  tb_FreqChange(self);
  rbTIG_DCClick(self);
  if not AcquireWakeLock then ShowMessage('������ ������� ���������� ������');
end;

procedure TForm1.IDCConnected(Sender: TObject);
var st:string;
begin
  memo1.Lines.Add('Connected');
  sleep(100);
  st:=sendCommand('v');
  memo1.Lines.Add(st);
  if pos('InvalidCommand',st)<>0 then
  begin
    memo1.Lines.Add(sendCommand('q'));
    st:= sendCommand('v');
    memo1.Lines.Add(st);
    L_firmware.Text:=st;
  end;
  if pos('SWM ',st)=0 then
  begin
    L_firmware.Text:='Error device connection (' + st + ')';
  end else
  begin
      L_firmware.Text:=st;
      if rbTIG_DC.IsChecked then
      begin
        ChangeParamsFlag:=true;
        thr:= TButtonThread.Create(false);
        rbTIG_DC.Enabled:=false;
        rbTIG_AC.Enabled:=false;
        RadioButton3.Enabled:=false;
      end;
      if rbTIG_AC.IsChecked then
      begin
        ChangeParamsFlag:=true;
        thr2:= TButtonThread_TIGAC.Create(false);
        rbTIG_DC.Enabled:=false;
        rbTIG_AC.Enabled:=false;
        RadioButton3.Enabled:=false;
      end
  end;
end;

procedure TForm1.IDCDisconnected(Sender: TObject);
begin
  memo1.Lines.Add('Disconnected');
end;

procedure TForm1.tb_tIminChange(Sender: TObject);
var st:string;
begin
  st:=floattostrF(tb_tImin.Value, ffFixed, 4, 1);
  L_tIMin.Text:=st;
  ChangeParams;
end;

procedure TForm1.tb_tStartPWMChange(Sender: TObject);
var st:string;
begin
  st:=floattostrF(tb_tStartPWM.Value, ffFixed, 4, 1);
  L_tStartPWM.Text:=st;
  ChangeParams;
end;

end.
